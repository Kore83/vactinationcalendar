﻿using System;

namespace Vactination.DomainModel
{
    public class VaccineFactory : IVaccineFactory
    {
        private readonly IVaccine allVaccines = new AllVaccines();
        private readonly IVaccine onlyRMantu = new OnlyRMantu();
        private readonly IVaccine onlyBcz = new OnlyBcz();

        public IVaccine GetVaccine(VaccinType vaccinType)
        {
            switch (vaccinType)
            {
                case VaccinType.All:
                    return allVaccines;
                    break;
                case VaccinType.OnlyRMantu:
                    return onlyRMantu;
                    break;
                case VaccinType.OnlyBcz:
                    return onlyBcz;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("vaccinType");
            }
        }
    }
}