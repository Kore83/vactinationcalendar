﻿namespace Vactination.DomainModel
{
    public struct DayInYear
    {
        public DayInYear(int year, int day) : this()
        {
            Year = year;
            Day = day;
        }

        public bool Equals(DayInYear other)
        {
            return Year == other.Year && Day == other.Day;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Year*397) ^ Day;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is DayInYear))
                return false;

            return Equals((DayInYear) obj);
        }

        public int Year { get; private set; }
        public int Day { get; private set; }
    }
}