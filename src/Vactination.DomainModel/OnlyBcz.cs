using System;
using System.Collections.Generic;
using Vactination.DomainModel.Helper;

namespace Vactination.DomainModel
{
    public class OnlyBcz : IVaccine
    {
        public IDictionary<DayInYear, WorkingDay> GetVactinantionDays(DateTime start, DateTime end)
        {
            var period = end - start;
            if (period.Days < 1)
                return new Dictionary<DayInYear, WorkingDay>();

            var vactionationDays = new Dictionary<DayInYear, WorkingDay>(period.Days);

            for (var day = 0; day < period.Days; day++)
            {
                var startDay = start.AddDays(day);
                switch (startDay.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        break;
                    case DayOfWeek.Monday:
                        break;
                    case DayOfWeek.Tuesday:
                        break;
                    case DayOfWeek.Wednesday:
                        break;
                    case DayOfWeek.Thursday:
                        var weekOfDay = startDay.DayOfWeekCount();
                        if (weekOfDay == 1 || weekOfDay == 3)
                        {
                            var thursdayWorkingDay = new WorkingDay(startDay, 10 * 60, 11 * 60);
                            vactionationDays.Add(startDay.ToDayInYear(), thursdayWorkingDay);
                        }
                        break;
                    case DayOfWeek.Friday:
                        break;
                    case DayOfWeek.Saturday:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return vactionationDays;
        }
    }
}