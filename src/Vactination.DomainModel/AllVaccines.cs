using System;
using System.Collections.Generic;
using Vactination.DomainModel.Helper;

namespace Vactination.DomainModel
{
    public class AllVaccines : IVaccine
    {
        public IDictionary<DayInYear, WorkingDay> GetVactinantionDays(DateTime start, DateTime end)
        {
            var period = end - start;
            if (period.Days < 1)
                return new Dictionary<DayInYear, WorkingDay>();

            var vactionationDays = new Dictionary<DayInYear, WorkingDay>(period.Days);

            for (var day = 0; day < period.Days; day++)
            {
                var startDay = start.AddDays(day);
                switch (startDay.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        break;
                    case DayOfWeek.Monday:
                        break;
                    case DayOfWeek.Tuesday:
                        var tuesdayWorkingDay = new WorkingDay(startDay, 9*60, 15*60);
                        vactionationDays.Add(startDay.ToDayInYear(), tuesdayWorkingDay);
                        break;
                    case DayOfWeek.Wednesday:
                        var wednesdayWorkingDay = new WorkingDay(startDay, 9 * 60, 15 * 60);
                        vactionationDays.Add(startDay.ToDayInYear(), wednesdayWorkingDay);
                        break;
                    case DayOfWeek.Thursday:
                        var weekOfDay = startDay.DayOfWeekCount();
                        if (weekOfDay == 2 || weekOfDay == 4 || weekOfDay == 5)
                        {
                            var thursdayWorkingDay = new WorkingDay(startDay, 9 * 60, 14 * 60);
                            vactionationDays.Add(startDay.ToDayInYear(), thursdayWorkingDay);
                        }
                        break;
                    case DayOfWeek.Friday:
                        var fridayWorkingDay = new WorkingDay(startDay, 9 * 60, 15 * 60);
                        vactionationDays.Add(startDay.ToDayInYear(), fridayWorkingDay);
                        break;
                    case DayOfWeek.Saturday:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return vactionationDays;
        }
    }
}