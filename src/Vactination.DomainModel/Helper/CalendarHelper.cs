using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Vactination.DomainModel.Helper
{
    public static class CalendarHelper
    {
        static private readonly DayOfWeek firstDay = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;

        public static int DayOfWeekCount(this DateTime dateTime)
        {
            return (dateTime.Day - 1)/7 + 1;
        }

        public static IEnumerable<DayOfWeek> Week()
        {
            var array = new DayOfWeek[7];
            for (int dayIndex = 0; dayIndex < 7; dayIndex++)
            {
                var currentDay = (DayOfWeek)(((int)firstDay + dayIndex) % 7);

                array[dayIndex] = currentDay;
            }

            return array;
        }

        public static IEnumerable<String> DayNamesInOrder()
        {
            return Week().Select(dayOfWeek => CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(dayOfWeek));
        }

        public static int DayNumber(this DayOfWeek dayOfWeek)
        {
            return (7 - (int) firstDay + (int) dayOfWeek) % 7 + 1;
        }

        public static DateTime GetFirstDayOfWeek(this DateTime date)
        {
            var diff = firstDay.DayNumber() - date.DayOfWeek.DayNumber();
            var firstDayOfWeek = date.AddDays(diff);
            return firstDayOfWeek;
        }

        public static DateTime GetLastDayOfWeek(this DateTime date)
        {
            var firstDayOfWeek = date.GetFirstDayOfWeek();
            return firstDayOfWeek.AddDays(7);
        }

        public static DateTime GetLastDayOfMonth(this DateTime date)
        {
            var lastDay = DateTime.DaysInMonth(date.Year, date.Month);
            return new DateTime(date.Year, date.Month, lastDay);
        }

        public static DateTime GetFirstDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        public static DayInYear ToDayInYear(this DateTime date)
        {
            return new DayInYear(date.Year, date.DayOfYear);
        }
    }
}