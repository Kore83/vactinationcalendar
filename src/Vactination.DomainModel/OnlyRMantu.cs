using System;
using System.Collections.Generic;
using Vactination.DomainModel.Helper;

namespace Vactination.DomainModel
{
    public class OnlyRMantu : IVaccine
    {
        public IDictionary<DayInYear, WorkingDay> GetVactinantionDays(DateTime start, DateTime end)
        {
            var period = end - start;
            if (period.Days < 1)
                return new Dictionary<DayInYear, WorkingDay>();

            var vactionationDays = new Dictionary<DayInYear, WorkingDay>(period.Days);

            for (var day = 0; day < period.Days; day++)
            {
                var startDay = start.AddDays(day);
                switch (startDay.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        break;
                    case DayOfWeek.Monday:
                        var mondayWorkingDay = new WorkingDay(startDay, 9*60, 15*60);
                        vactionationDays.Add(startDay.ToDayInYear(), mondayWorkingDay);
                        break;
                    case DayOfWeek.Tuesday:
                        break;
                    case DayOfWeek.Wednesday:
                        break;
                    case DayOfWeek.Thursday:
                        break;
                    case DayOfWeek.Friday:
                        break;
                    case DayOfWeek.Saturday:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return vactionationDays;
        }
    }
}