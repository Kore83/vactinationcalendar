﻿namespace Vactination.DomainModel
{
    public interface IVaccineFactory
    {
        IVaccine GetVaccine(VaccinType vaccinType);
    }
}