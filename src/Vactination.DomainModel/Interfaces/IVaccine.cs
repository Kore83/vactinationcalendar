﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vactination.DomainModel
{
    public interface IVaccine
    {
        IDictionary<DayInYear, WorkingDay> GetVactinantionDays(DateTime start, DateTime end);
    }
}
