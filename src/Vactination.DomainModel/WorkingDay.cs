﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vactination.DomainModel
{
    public class WorkingDay
    {
        public WorkingDay(DateTime visitingDay, int startHourInMinutes, int endHourInMinutes)
        {
            if (startHourInMinutes < 0 || startHourInMinutes > 24*60)
                throw new ArgumentOutOfRangeException("startHourInMinutes");

            if (endHourInMinutes < 0 || endHourInMinutes > 24*60)
                throw new ArgumentOutOfRangeException("endHourInMinutes");

            if (startHourInMinutes > endHourInMinutes)
                throw new ArgumentOutOfRangeException("startHour must be less than endHour");

            this.StartHourInMinutes = startHourInMinutes;
            this.EndHourInMinutes = endHourInMinutes;
            this.VistingDay = visitingDay;
            
        }

        public DateTime VistingDay { get; private set; }
        public int StartHourInMinutes { get; private set; }
        public int EndHourInMinutes { get; private set; }

        public int HoursOfWork
        {
            get { return (EndHourInMinutes - StartHourInMinutes) / 60; }
        }
    }
}
