using System;
using GalaSoft.MvvmLight;
using Vactination.DomainModel;

namespace Vactination.PresentationLogic
{
    public class DayInCalendar : ViewModelBase
    {
        private DateTime day;

        public DayInCalendar()
        {
            
        }

        public DayInCalendar(WorkingDay workingDay)
        {
            if (workingDay == null)
                throw new ArgumentNullException("workingDay");
            this.Day = workingDay.VistingDay;
        }

        public DateTime Day
        {
            get { return day; }
            set { day = value; this.RaisePropertyChanged(()=>Day); }
        }

        public int WorkingPeriodInMinutes { get; set; }
    }
}