﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Vactination.DomainModel;

namespace Vactination.PresentationLogic
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IVaccineFactory vaccineFactory;
        private ICommand setAllVaccinesCommand;
        private ICommand setOnlyRMantuCommand;
        private ICommand setOnlyBczCommand;
        private ICommand nextMonthCommand;
        private ICommand prevMonthCommand;
        private DateTime currentMonth;
        private IDictionary<DayInYear, WorkingDay> workingDays;

        public MainViewModel(IVaccineFactory vaccineFactory)
        {
            if (vaccineFactory == null)
                throw new ArgumentNullException("vaccineFactory");

            this.vaccineFactory = vaccineFactory;
            this.currentMonth = DateTime.Today;
            this.CurrentVaccine = vaccineFactory.GetVaccine(VaccinType.All);
        }

        public ICommand SetAllVaccinesCommand
        {
            get { return this.setAllVaccinesCommand ?? (this.setAllVaccinesCommand = new RelayCommand(() => SetVaccine(VaccinType.All))); }
        }

        public ICommand SetOnlyRMantuCommand
        {
            get { return this.setOnlyRMantuCommand ?? (this.setOnlyRMantuCommand = new RelayCommand(() => SetVaccine(VaccinType.OnlyRMantu))); }
        }

        public ICommand SetOnlyBczCommand
        {
            get { return this.setOnlyBczCommand ?? (this.setOnlyBczCommand = new RelayCommand(() => SetVaccine(VaccinType.OnlyBcz))); }
        }

        public IVaccine CurrentVaccine { get; set; }

        public ICommand NextMonthCommand
        {
            get { return nextMonthCommand ?? (this.nextMonthCommand = new RelayCommand(() => AddMonth(1), CanNextMonthExecute)); }
        }

        private void AddMonth(int months)
        {
            this.CurrentMonth = this.CurrentMonth.AddMonths(months);
        }

        public DateTime CurrentMonth
        {
            get
            {
                return currentMonth;
            }
            set
            {
                if (currentMonth == value)
                    return;
                currentMonth = value;
                this.Refresh();
                this.RaisePropertyChanged(()=>CurrentMonth);
            }
        }

        private bool CanNextMonthExecute()
        {
            return true;
        }

        public ICommand PreviousMonthCommand
        {
            get { return prevMonthCommand ?? (this.prevMonthCommand = new RelayCommand(()=>AddMonth(-1), CanPrevMonthExecute)); }
        }

        public IDictionary<DayInYear, WorkingDay> WorkingDays
        {
            get { return workingDays; }
            set { workingDays = value; this.RaisePropertyChanged(()=>WorkingDays); }
        }

        private bool CanPrevMonthExecute()
        {
            return (CurrentMonth > DateTime.Today);
        }

        private void SetVaccine(VaccinType vaccinType)
        {
            this.CurrentVaccine = this.vaccineFactory.GetVaccine(vaccinType);
            this.Refresh();
        }

        private void Refresh()
        {
            this.WorkingDays = this.CurrentVaccine.GetVactinantionDays(this.currentMonth.AddMonths(-1), this.currentMonth.AddMonths(2));
        }
    }
}
