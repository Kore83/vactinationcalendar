﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Autofac;
using Autofac.Core;
using Vactination.DomainModel;
using Vactination.PresentationLogic;

namespace Vactination.Wpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var builder = new ContainerBuilder();
            builder.RegisterType<VaccineFactory>().As<IVaccineFactory>().SingleInstance();
            builder.RegisterType<MainViewModel>().AsSelf().SingleInstance();
            var container = builder.Build();

            var mainVm = container.Resolve<MainViewModel>();
            var mainView = new MainWindow() { DataContext = mainVm };
            mainView.Show();
        }
    }
}
