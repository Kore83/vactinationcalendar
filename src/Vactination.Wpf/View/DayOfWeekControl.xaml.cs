﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Vactination.DomainModel;

namespace Vactination.Wpf.View
{
    /// <summary>
    /// Interaction logic for DayOfWeekControl.xaml
    /// </summary>
    public partial class DayOfWeekControl : UserControl
    {
        public static readonly DependencyProperty WeekDayProperty =
            DependencyProperty.Register("WeekDay", typeof (System.DayOfWeek), typeof (DayOfWeekControl), new PropertyMetadata(default(System.DayOfWeek)));

        public System.DayOfWeek WeekDay
        {
            get { return (System.DayOfWeek) GetValue(WeekDayProperty); }
            set { SetValue(WeekDayProperty, value); }
        }

        public static readonly DependencyProperty DaysProperty =
            DependencyProperty.Register("Days", typeof(IEnumerable<WorkingDay>), typeof(DayOfWeekControl), new PropertyMetadata(default(IEnumerable<WorkingDay>)));

        public IEnumerable<WorkingDay> Days
        {
            get { return (IEnumerable<WorkingDay>)GetValue(DaysProperty); }
            set { SetValue(DaysProperty, value); }
        }

        public DayOfWeekControl()
        {
            InitializeComponent();
        }
    }
}
