﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Vactination.DomainModel;
using Vactination.DomainModel.Helper;

namespace Vactination.Wpf.View
{
    /// <summary>
    /// Interaction logic for CustomCalendar.xaml
    /// </summary>
    public partial class CustomCalendar : UserControl
    {
        public static readonly DependencyProperty MonthProperty =
            DependencyProperty.Register("CurrentMonth", typeof (DateTime), typeof (CustomCalendar), new PropertyMetadata(default(DateTime), MonthPropertyChanged));

        private static void MonthPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var calendar = (CustomCalendar) dependencyObject;
            calendar.Refresh();
        }

        public static readonly DependencyProperty WorkingDaysProperty =
            DependencyProperty.Register("WorkingDays", typeof (IDictionary<DayInYear, WorkingDay>), typeof (CustomCalendar), new PropertyMetadata(default(IEnumerable<WorkingDay>), WorkingDaysPropertyChanged));

        private static void WorkingDaysPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var calendar = (CustomCalendar)dependencyObject;
            calendar.Refresh();
        }

        public IDictionary<DayInYear, WorkingDay> WorkingDays
        {
            get
            {
                return (IDictionary<DayInYear, WorkingDay>)GetValue(WorkingDaysProperty);
            }
            set
            {
                SetValue(WorkingDaysProperty, value);
            }
        }

        public DateTime CurrentMonth
        {
            get
            {
                return (DateTime) GetValue(MonthProperty);
            }
            set
            {
                SetValue(MonthProperty, value);
            }
        }

        public List<WorkingDay> AllDays { get; private set; }

        public CustomCalendar()
        {
            InitializeComponent();
        }

        private void Refresh()
        {
            if (this.WorkingDays == null)
                return;

            var firstDayOfMonth = this.CurrentMonth.GetFirstDayOfMonth();
            var firstDay = firstDayOfMonth.GetFirstDayOfWeek();
            var lastDay = this.CurrentMonth.GetLastDayOfMonth().GetLastDayOfWeek();

            var period = (lastDay - firstDay).Days;
            this.AllDays = new List<WorkingDay>(period);

            for (int i = 0; i < period; i++)
            {
                var startDay = firstDay.AddDays(i);
                var dayInYear = startDay.ToDayInYear();
                if (startDay >= DateTime.Today && this.WorkingDays.ContainsKey(dayInYear))
                {
                    this.AllDays.Add(this.WorkingDays[dayInYear]);
                }
                else
                {
                    this.AllDays.Add(new WorkingDay(startDay, 0, 0));
                }
            }

            var data = new List<DayInWeek>(7);

            foreach (var dayOfWeek in CalendarHelper.Week())
            {
                var dayInWeek = new DayInWeek()
                    {
                        DayOfWeek = dayOfWeek, 
                        Days = this.AllDays.Where(date => date.VistingDay.DayOfWeek == dayOfWeek).ToList()
                    };
                data.Add(dayInWeek);
            }

            calendar.ItemsSource = data;
        }
    }

    class DayInWeek
    {
        public DayOfWeek DayOfWeek { get; set; }
        public IEnumerable<WorkingDay> Days { get; set; }
    }
}
