﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Vactination.Wpf.Converters
{
    public class HoursToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var hours = (int) value;

            if (hours <= 0)
                return new SolidColorBrush(Color.FromArgb(100, 100, 100, 100));
            if (hours <= 1)
                return new SolidColorBrush(Color.FromArgb(200, 245, 120, 60));
            if (hours <= 5)
                return new SolidColorBrush(Color.FromArgb(200, 240, 240, 50));

            return new SolidColorBrush(Color.FromArgb(200, 10, 200, 10));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
